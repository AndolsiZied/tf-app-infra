module "compute" {
  source = "./modules/compute"

  namespace = var.namespace
  project   = var.project

  vpc = module.network.vpc
}

module "network" {
  source = "./modules/network"

  namespace = var.namespace
  project   = var.project
}