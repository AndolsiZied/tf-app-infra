variable "project" {
  description = "The project ID where all resources will be launched."
  type        = string
}

variable "region" {
  description = "GCP region"
  type        = string
  default     = "us-central1"
}

variable "namespace" {
  description = "Used to group resources"
  type        = string
}

variable "database_version" {
  description = "Database version"
  type        = string
  default     = "POSTGRES_11"
}

variable "vpc" {
  description = "The VPC to be used."
  type        = any
}

variable "host" {
  description = "The host the user can connect from"
  type        = string
  default     = ""
}