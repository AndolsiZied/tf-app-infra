resource "random_password" "su-password" {
  length           = 16
  special          = true
  override_special = "_%@/'\""
}

resource "random_password" "ro-password" {
  length  = 8
  special = false
}

resource "random_password" "rw-password" {
  length  = 8
  special = false
}

resource "google_sql_database_instance" "database" {
  name             = "${var.namespace}-db-instance"
  database_version = var.database_version
  region           = var.region
  project          = var.project

  settings {
    tier      = "db-f1-micro"
    disk_type = "PD_SSD"
    ip_configuration {
      ipv4_enabled    = false
      private_network = var.vpc.self_link
    }
  }
}

resource "google_sql_user" "super_user" {
  name     = "${var.namespace}-super-user"
  instance = google_sql_database_instance.database.name
  host     = var.host
  password = random_password.su-password.result
}

resource "google_sql_user" "rw_user" {
  name     = "${var.namespace}-rw-user"
  instance = google_sql_database_instance.database.name
  host     = var.host
  password = random_password.rw-password.result
}

resource "google_sql_user" "ro_user" {
  name     = "${var.namespace}-ro-user"
  instance = google_sql_database_instance.database.name
  host     = var.host
  password = random_password.ro-password.result
}