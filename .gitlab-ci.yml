image:
  name: hashicorp/terraform:0.15.5
  entrypoint: [""]

cache:
  key: "tf-deployer-${CI_PIPELINE_ID}"
  paths:
    - .terraform/
    - .terraform.lock.hcl

stages:
  - init
  - plan_dev
  - apply_dev
  - plan_qa
  - apply_qa
  - plan_prod
  - apply_prod
  - destroy_plan
  - destroy
  - unlock

.tf_deployer_tags:
  tags:
    - gitlab-org-docker

.build:
  except:
    variables:
      - ($DESTROY || $UNLOCK)

.destroy:
  only:
    variables:
      - $DESTROY

.unlock:
  only:
    variables:
      - $UNLOCK

.auth_before_script:
  before_script:
    - echo ${GCLOUD_SERVICE_KEY} > /tmp/service-account.json
    - export GOOGLE_APPLICATION_CREDENTIALS=/tmp/service-account.json

.tf_init:
  script:
    - terraform init -backend-config="bucket=tf-interview-states-bucket" -backend-config="prefix=app-infra"
  extends:
    - .auth_before_script
    - .build

.tf_plan:
  script:
    - terraform workspace select ${ENV} || terraform workspace new ${ENV}
    - terraform plan -var-file=env/${ENV}.tfvars
  extends:
    - .auth_before_script
    - .build

.tf_apply:
  script:
    - terraform workspace select ${ENV}
    - terraform apply -var-file=env/${ENV}.tfvars -auto-approve
  extends:
    - .auth_before_script
    - .build

.dev_context:
  variables:
    ENV: dev

.qa_context:
  variables:
    ENV: qa

.prod_context:
  variables:
    ENV: prod

init:
  stage: init
  extends:
    - .tf_deployer_tags
    - .tf_init

plan_dev:
  stage: plan_dev
  extends:
    - .tf_deployer_tags
    - .dev_context
    - .tf_plan

apply_dev:
  stage: apply_dev
  extends:
    - .tf_deployer_tags
    - .dev_context
    - .tf_apply
  when: manual

plan_qa:
  stage: plan_qa
  extends:
    - .tf_deployer_tags
    - .qa_context
    - .tf_plan

apply_qa:
  stage: apply_qa
  extends:
    - .tf_deployer_tags
    - .qa_context
    - .tf_apply
  when: manual

plan_prod:
  stage: plan_prod
  extends:
    - .tf_deployer_tags
    - .prod_context
    - .tf_plan

apply_prod:
  stage: apply_prod
  extends:
    - .tf_deployer_tags
    - .prod_context
    - .tf_apply
  when: manual

prepare_destroy:
  stage: destroy_plan
  script:
    - terraform init -backend-config="bucket=tf-interview-states-bucket" -backend-config="prefix=app-infra"
    - terraform workspace select ${ENV}
    - terraform plan -destroy -var-file=env/${ENV}.tfvars
  extends:
    - .auth_before_script
    - .destroy

destroy:
  stage: destroy
  script:
    - terraform workspace select ${ENV}
    - terraform destroy -var-file=env/${ENV}.tfvars -auto-approve
  when: manual
  extends:
    - .auth_before_script
    - .destroy

unlock:
  stage: unlock
  script:
    - terraform init -backend-config="bucket=tf-interview-states-bucket" -backend-config="prefix=app-infra"
    - terraform workspace select ${ENV}
    - terraform force-unlock -force ${TF_LOCKID}
  extends:
    - .auth_before_script
    - .unlock