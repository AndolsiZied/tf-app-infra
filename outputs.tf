output "cluster_endpoint" {
  description = "The IP address of the cluster master."
  value       = module.compute.cluster_endpoint
}